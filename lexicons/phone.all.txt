xj	xj
ʃ	ʃ
<prompt>	<prompt>
oʊ	oʊ
əɪ̪	əɪ̪
<cough>	<cough>
mj	mj
aːʊ̪	aːʊ̪
u̯ʊ	u̯ʊ
ʦj	ʦj
<hes>	<hes>
gj	gj
ũ	ũ
ʊ̃	ʊ̃
ʣ	ʣ
ɔɪ̪	ɔɪ̪
<int>	<int>
ʤj	ʤj
c	c
lj	lj
ʧj	ʧj
æ̃	æ̃
tj	tj
oaɪ̪ː	oaɪ̪ː
th	th
ɣ	ɣ
øː	øː
oː	oː
eː	eː
nj	nj
ǁ	ǁ
tʼ	tʼ
tɕ	tɕ
dj	dj
dh	dh
χ	χ
iə̯	iə̯
iə̪	iə̪
ɛ̃	ɛ̃
ɐ	ɐ
u~	u~
ɻ	ɻ
ɔ	ɔ
<ring>	<ring>
u̪e	u̪e
ɘ	ɘ
ɜ	ɜ
ʧh	ʧh
ɠ	ɠ
uːɪ̪	uːɪ̪
tʂ	tʂ
d	d
h	h
ɦ	ɦ
l	l
eɐ	eɐ
p	p
t	t
<dtmf>	<dtmf>
ɐʊ	ɐʊ
x	x
ɯː	ɯː
uə̪	uə̪
uə̯	uə̯
ɤ	ɤ
ɪ	ɪ
ɥi	ɥi
iɘ̯	iɘ̯
ʤh	ʤh
eɪ	eɪ
ʈ	ʈ
ʧʼ	ʧʼ
ʌ	ʌ
ɬ	ɬ
ʐ	ʐ
e~	e~
<laugh>	<laugh>
ʔ	ʔ
ɡǃɦ	ɡǃɦ
ɐɪ	ɐɪ
ɮ	ɮ
ʃj	ʃj
uɪ	uɪ
ʤ	ʤ
ɰ	ɰ
uə̪ɪ̪	uə̪ɪ̪
<lipsmack>	<lipsmack>
y̯ʉ	y̯ʉ
r	r
uɪ̪	uɪ̪
gh	gh
ɨə̪ɪ̪	ɨə̪ɪ̪
<sta>	<sta>
rj	rj
ɨʊ̪	ɨʊ̪
ʋ	ʋ
ɔaː	ɔaː
t͡ʃ	t͡ʃ
bh	bh
bj	bj
ɓ	ɓ
æː	æː
ɗ	ɗ
ɳ	ɳ
ɛ	ɛ
ɨə̪ʊ̪	ɨə̪ʊ̪
ʃjː	ʃjː
ɟ	ɟ
oɐ	oɐ
n̪	n̪
ǃh	ǃh
iə̪ʊ̪	iə̪ʊ̪
ʧ	ʧ
oi	oi
k	k
o	o
<overlap>	<overlap>
ð	ð
s	s
w	w
ɡ|ɦ	ɡ|ɦ
d̪h	d̪h
ĩ	ĩ
aʊ	aʊ
ʝh	ʝh
ɤː	ɤː
ʁ	ʁ
ɣj	ɣj
d̪	d̪
ʉ	ʉ
ɛː	ɛː
ʕ	ʕ
əː	əː
<foreign>	<foreign>
ʝ	ʝ
əːɪ̪	əːɪ̪
a~	a~
i	i
iː	iː
əʊ̪	əʊ̪
ɔː	ɔː
<breath>	<breath>
aː	aː
pj	pj
ph	ph
ɡ	ɡ
ǃ	ǃ
aɪ	aɪ
<no-speech>	<no-speech>
eʊ̪	eʊ̪
ɨ	ɨ
ɨə̪	ɨə̪
ã	ã
aʊ̪	aʊ̪
ɖ	ɖ
oɪ̪	oɪ̪
ʒj	ʒj
oɪ	oɪ
ɑː	ɑː
b	b
g	g
f	f
ǁh	ǁh
j	j
l̪	l̪
n	n
oũ	oũ
ɲ	ɲ
t̪	t̪
v	v
ɛʊ̪	ɛʊ̪
ɔɪ	ɔɪ
z	z
ɫ	ɫ
zj	zj
ɾ	ɾ
ɔ~	ɔ~
ʂ	ʂ
<click>	<click>
aːɪ̪	aːɪ̪
ʣj	ʣj
ai	ai
t̪h	t̪h
vj	vj
ʊ	ʊ
õ	õ
yː	yː
ɨɪ̪	ɨɪ̪
ʒ	ʒ
au	au
ɯ	ɯ
k’	k’
ǀh	ǀh
ɔa	ɔa
<female-to-male>	<female-to-male>
oaɪ̪	oaɪ̪
ɔɛ	ɔɛ
<male-to-female>	<male-to-female>
ʦ	ʦ
pʼ	pʼ
aɪ̪	aɪ̪
ɡǁɦ	ɡǁɦ
d͡ʒ	d͡ʒ
θ	θ
iʊ̪	iʊ̪
iʊ	iʊ
uə̪ː	uə̪ː
ɭ	ɭ
Ɂ	Ɂ
ǀ	ǀ
ŋ	ŋ
uː	uː
uɐ	uɐ
ɑ	ɑ
ɹ	ɹ
kʟ	kʟ
ø	ø
ə	ə
fj	fj
ou	ou
ɑo	ɑo
a	a
ɯə̯	ɯə̯
e	e
ħ	ħ
æ	æ
ɖh	ɖh
uɪə̪	uɪə̪
kh	kh
m	m
ẽ	ẽ
ɔ̃	ɔ̃
q	q
sj	sj
ʈh	ʈh
kj	kj
u	u
iɐ	iɐ
y	y
ɽ	ɽ
