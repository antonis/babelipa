# !/usr/bin/python
import os
import codecs
import numpy as np
import argparse
import epitran
import string
import regex as re

epi = epitran.Epitran('tur-Latn')

def remove_punctuation(text):
    return re.sub(ur"\p{P}+", "", text)

def readLexicon(f):
	with codecs.open(f, 'r', 'utf-8') as inf:
		lines = inf.readlines()
	lexicon = {}
	for l in lines:
		l = (l.strip()).split('\t')
		
		word = l[0]
		pron = l[-1]
		lexicon[word] = pron.split()

	return lexicon

def readMapping(f):
	with codecs.open(f, 'r', 'utf-8') as inf:
		lines = inf.readlines()
	mapping = {}
	for l in lines:
		l = (l.strip()).split()
		mapping[l[1]] = l[0]
	return mapping

phoneDictionary = {}

def convert2IPA(outf, lines, lex, mapping):
	of = codecs.open(outf, 'w', 'utf-8')
	for line in lines:
		line = line.strip()
		if line[0] == '[':
			of.write(line + '\n')
		else:
			line = line.split()
			for word in line:
				if word[0] == '<':
					of.write(word + ' ')
					if word not in phoneDictionary:
						phoneDictionary[word] = word
				elif word in lex:
					for phone in lex[word]:
						if (phone == '.') or (phone == '#') or (phone == '\"'):
							pass
						else:
							of.write(mapping[phone] + ' ')
							if mapping[phone] not in phoneDictionary:
								phoneDictionary[mapping[phone]] = mapping[phone]
				elif (word[0]=='*') and (word[-1]=='*'):
					if word[1:-1] in lex:
						for phone in lex[word[1:-1]]:
							if (phone == '.') or (phone == '#') or (phone == '\"'):
								pass
							else:
								of.write(mapping[phone] + ' ')
								if mapping[phone] not in phoneDictionary:
									phoneDictionary[mapping[phone]] = mapping[phone]
					else:
						for t in epi.word_to_tuples(word[1:-1]):
							of.write(t[3] + ' ')
				elif (word[-1]=='-'):
					if word[:-1] in lex:
						for phone in lex[word[:-1]]:
							if (phone == '.') or (phone == '#') or (phone == '\"'):
								pass
							else:
								of.write(mapping[phone] + ' ')
								if mapping[phone] not in phoneDictionary:
									phoneDictionary[mapping[phone]] = mapping[phone]
					else:
						for t in epi.word_to_tuples(word[:-1]):
							of.write(t[3] + ' ')
							if t[3] not in phoneDictionary:
								phoneDictionary[t[3]] = t[3]
				elif (word not in lex):
					cleanword = remove_punctuation(word)
					if len(cleanword) > 0:
						if cleanword in lex:
							for phone in lex[word[1:-1]]:
								if (phone == '.') or (phone == '#') or (phone == '\"'):
									pass
								else:
									of.write(mapping[phone] + ' ')
									if mapping[phone] not in phoneDictionary:
										phoneDictionary[mapping[phone]] = mapping[phone]
						else:
							for t in epi.word_to_tuples(cleanword):
								of.write(t[3] + ' ')
								if t[3] not in phoneDictionary:
									phoneDictionary[t[3]] = t[3]
						with codecs.open("OOV.txt", 'a', 'utf-8') as oovf:
							oovf.write(cleanword + '\n')
			of.write('\n')
	of.close()



def main():
	parser = argparse.ArgumentParser(description='Read in a transcription dir, a lexicon, and output the IPA transcriptions.')
	parser.add_argument('-i', required=True, type=str, help='Input transcriptions directory')
	parser.add_argument('-o', required=True, type=str, help='Output directory')
	parser.add_argument('-l', required=True, type=str, help='Lexicon file')
	parser.add_argument('-m', required=True, type=str, help='Mapping to IPA file')
	args = parser.parse_args()
	inpf = args.i
	outf = args.o
	lexf = args.l
	mapf = args.m

	lexicon = readLexicon(lexf)
	mapping = readMapping(mapf)

	try:
		os.mkdir(outf)
	except:
		pass

	for filename in os.listdir(inpf):
		with codecs.open(os.path.join(inpf, filename), 'r', 'utf-8') as inf:
			inlines = inf.readlines()
		convert2IPA(os.path.join(outf, filename), inlines, lexicon, mapping)

	with codecs.open("105-turkish/phone.lexicon.txt", 'w', 'utf-8') as phlf:
		for phone in phoneDictionary:
			phlf.write(phone + '\t' + phone + '\n')
	



if __name__ == "__main__":
	main()
