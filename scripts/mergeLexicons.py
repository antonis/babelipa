# !/usr/bin/python
import os
import codecs
import argparse

def remove_punctuation(text):
    return re.sub(ur"\p{P}+", "", text)

def readLexicon(f):
	with codecs.open(f, 'r', 'utf-8') as inf:
		lines = inf.readlines()
	lexicon = {}
	for l in lines:
		l = (l.strip()).split('\t')
		
		word = l[0]
		pron = l[-1]
		lexicon[word] = pron.split()

	return lexicon



def main():
	parser = argparse.ArgumentParser(description='Read in a list of lexicons, and merge them into one.')
	parser.add_argument('i', type=str, nargs='*', help='Input transcriptions directory')
	parser.add_argument('o', type=str, nargs=1, help='Output lexicon')
	args = parser.parse_args()
	inpf = args.i
	outf = args.o
	
	mergedDic = {}
	lexlist = []
	# Read the lexicons and get unique values
	for f in inpf:
		lex = readLexicon(f)
		# store the lexicons for stat purposes
		lexlist.append(lex)
		for phone in lex:
			# keep number of occurences for stat purposes.
			# The phones are the keys
			if not (phone in mergedDic):
				mergedDic[phone] = 1
			else:
				mergedDic[phone] += 1


	# Write the output
	with codecs.open(outf[0], 'w', 'utf-8') as outlex:
		for phone in mergedDic:
			outlex.write(phone + '\t' + phone + '\n')

	# Stats
	print "Total (merged) phonemes: ", len(mergedDic)
	count = 0
	for phone in mergedDic:
		found = 1
		for lex in lexlist:
			if not phone in lex:
				found = 0
				break
		if found:
			count += 1
	print "Overlapping phonemes across all languages: ", count, " ( ", float(count)*100/len(mergedDic), " % )"
	for i,lex in enumerate(lexlist):
		uniq = 0
		for phone in lex:
			if mergedDic[phone] == 1:
				uniq += 1
		print "Unique phonemes in ", inpf[i], " : ", uniq,  " ( ", float(uniq)*100/len(mergedDic), " % )"

	for i in range(2,len(lexlist)):
		count = 0
		for phone in mergedDic:
			if mergedDic[phone] == i:
				count += 1
		print "Number of phonemes that appear in ", i, " languages: ", count, " ( ", float(count)*100/len(mergedDic), " % )"



	

if __name__ == "__main__":
	main()
