# !/usr/bin/python
import os
import codecs
import numpy as np
import argparse
import epitran
import string
import regex as re

epi = epitran.Epitran('ind-Latn')

def remove_punctuation(text):
    return re.sub(ur"\p{P}+", "", text)

def readLexicon(f):
	with codecs.open(f, 'r', 'utf-8') as inf:
		lines = inf.readlines()
	lexicon = {}
	for l in lines:
		l = (l.strip()).split('\t')
		
		word = l[0]
		pron = l[-1]
		lexicon[word] = pron.split()

	return lexicon

def readMapping(f):
	with codecs.open(f, 'r', 'utf-8') as inf:
		lines = inf.readlines()
	mapping = {}
	for l in lines:
		l = (l.strip()).split()
		mapping[l[1]] = l[0]
	return mapping

phoneDictionary = {}

def convert2IPA(outf, lines, lex, mapping, stm):
	of = codecs.open(outf, 'w', 'utf-8')
	#print outf
	AllSentCount = 0.0
	OOVSentCount = 0.0
	for line in lines:
		line = line.strip()
		if line[0] == '[':
			of.write(line + '\n')
		else:
			line = line.split()
			if stm:
				of.write(" ".join(line[:5]) + " ")
				line = line[5:]
			AllSentCount += 1
			for word in line:
				if word[0] == '<':
					of.write(word + ' ')
					if word not in phoneDictionary:
						phoneDictionary[word] = word
				elif word in lex:
					#print word, lex[word]
					for phone in lex[word]:
						if (phone == '.') or (phone == '%') or (phone == '\"') or (phone== '#'):
							pass
						# Ignore tonal information
						elif '_' in phone:
							k = phone.index('_')
							if len(phone[:k]) > 0:
								of.write(mapping[phone[:k]] + ' ')
						#elif '_' == phone[0]:
							#pass
						else:
							of.write(mapping[phone] + ' ')
							if mapping[phone] not in phoneDictionary:
								phoneDictionary[mapping[phone]] = mapping[phone]
				elif (word[0]=='*') and (word[-1]=='*'):
					if word[1:-1] in lex:
						for phone in lex[word[1:-1]]:
							if (phone == '.') or (phone == '%') or (phone == '\"') or (phone== '#'):
								pass
							# Ignore tonal information
							elif '_' in phone:
								k = phone.index('_')
								if len(phone[:k]) > 0:
									of.write(mapping[phone[:k]] + ' ')
							#elif '_' == phone[0]:
								#pass
							else:
								of.write(mapping[phone] + ' ')
								if mapping[phone] not in phoneDictionary:
									phoneDictionary[mapping[phone]] = mapping[phone]
					else:
						of.write('<oov> ')
						OOVSentCount += 1
				elif (word[-1]=='-'):
					if word[:-1] in lex:
						for phone in lex[word[:-1]]:
							if (phone == '.') or (phone == '#') or (phone == '\"') or (phone== '%'):
								pass
							# Ignore tonal information
							elif '_' in phone:
								k = phone.index('_')
								if len(phone[:k])>0:
									of.write(mapping[phone[:k]] + ' ')
							#elif '_' == phone[0]:
								#pass
							else:
								of.write(mapping[phone] + ' ')
								if mapping[phone] not in phoneDictionary:
									phoneDictionary[mapping[phone]] = mapping[phone]
					else:
						of.write('<oov> ')
						OOVSentCount += 1
				elif (word not in lex):
					cleanword = remove_punctuation(word)
					if len(cleanword) > 0:
						if cleanword in lex:
							for phone in lex[cleanword]:
								if (phone == '.') or (phone == '#') or (phone == '\"') or (phone== '%'):
									pass
								# Ignore tonal information
								elif '_' in phone:
									k = phone.index('_')
									if len(phone[:k])>0:
										of.write(mapping[phone[:k]] + ' ')
								#elif '_' == phone[0]:
								#	pass
								else:
									of.write(mapping[phone] + ' ')
									if mapping[phone] not in phoneDictionary:
										phoneDictionary[mapping[phone]] = mapping[phone]
						else:
							of.write('<oov> ')
							OOVSentCount += 1
			of.write('\n')
	of.close()
	return AllSentCount, OOVSentCount



def main():
	parser = argparse.ArgumentParser(description='Read in a transcription dir, a lexicon, and output the IPA transcriptions.')
	parser.add_argument('-i', required=True, type=str, help='Input transcriptions directory')
	parser.add_argument('-o', required=True, type=str, help='Output ipa directory')
	parser.add_argument('-l', required=True, type=str, help='Lexicon file')
	parser.add_argument('-k', required=True, type=str, help='Output phone lexicon file')
	parser.add_argument('-m', required=True, type=str, help='Mapping to IPA file')
	parser.add_argument('-stm', action='store_true', help='Flag to indicate that the input transcriptions are STM files, which are used for sclite scoring.')
	args = parser.parse_args()
	inpf = args.i
	outf = args.o
	lexf = args.l
	mapf = args.m
	outlexf = args.k
	stm = args.stm

	lexicon = readLexicon(lexf)
	mapping = readMapping(mapf)
	print len(lexicon)
	print len(mapping)
	AllSentCount = 0.0
	OOVSentCount = 0.0

	if stm:
		# Don't assume a directory, but a single input file.
		with codecs.open(inpf, 'r', 'utf-8') as inf:
			inlines = inf.readlines()
		a,o = convert2IPA(outf, inlines, lexicon, mapping, stm)
		AllSentCount += a
		OOVSentCount += o
	else: 
		# Assume a directory input file with a stack of *inLine.txt and *outLine.txt files.
		try:
			os.mkdir(outf)
		except:
			pass
		for filename in os.listdir(inpf):
			with codecs.open(os.path.join(inpf, filename), 'r', 'utf-8') as inf:
				inlines = inf.readlines()
			a,o = convert2IPA(os.path.join(outf, filename), inlines, lexicon, mapping, stm)
			AllSentCount += a
			OOVSentCount += o

	with codecs.open(outlexf, 'w', 'utf-8') as phlf:
		for phone in phoneDictionary:
			phlf.write(phone + '\t' + phone + '\n')
	print AllSentCount, OOVSentCount, OOVSentCount/AllSentCount



if __name__ == "__main__":
	main()
